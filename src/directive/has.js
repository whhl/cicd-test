export default {
    bind(el, bindings) {
        const role = bindings.value // 用户角色 v-role = 'admin'
        const permissions = ['admin','user'] // 接口返回的当前用户登陆的权限列表
        if(!permissions.includes(role)) {
            // 如果没有权限，则移除按钮
           setTimeout(() => {
            // 转变为宏任务
            el.parentNode.removeChild(el)
           }, 0);
        }
    }
}