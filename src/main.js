import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

// 原始数据
let sourece = {
  a: 1,
  b: 2,
  c: {
    c1: 'c1'
  }
}
// 深拷贝，指向新的内存地址（新对象）,不受原数据影响
let obj = JSON.parse(JSON.stringify(sourece))
// 浅拷贝，引用关系，指向同一个内存地址，受原数据影响
let obj2 = Object.assign(sourece,{})
sourece.a = 111
sourece.b = 222


console.log(obj,obj2)




new Vue({
  render: h => h(App)
}).$mount('#app')